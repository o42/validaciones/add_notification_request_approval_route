# -*- coding: utf-8 -*-
{
    'name': 'Notificaciones cron pedidos compra(purchase_approval_route)',
    'version': '1.0.0',
    'summary': """
    Se agrega notificaciones de tareas
    """,
    'category': 'Purchases',
    'author': 'Odoo',
    'support': 'odoo',
    'license': 'OPL-1',
    'description':
        """
        Notificaciones
        """,
    'data': [
        'security/ir.model.access.csv',
        'data/days_week_data.xml',
        'data/ir_cron.xml',
        'data/templates.xml',
        'views/notification_team_approver_views.xml',
    ],
    'depends': ['extends_purchase_approval_route'],
    'installable': True,
    'auto_install': True,
    'application': False,
}
