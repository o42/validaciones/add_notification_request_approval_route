# -*- coding: utf-8 -*-
from odoo import api, fields, models
from datetime import datetime, timedelta, time

from odoo.tools.safe_eval import pytz

TIMES = [
        ('0', '12:00 AM'), ('1', '1:00 AM'), ('2', '2:00 AM'),
        ('3', '3:00 AM'), ('4', '4:00 AM'), ('5', '5:00 AM'),
        ('6', '6:00 AM'), ('7', '7:00 AM'), ('8', '8:00 AM'),
        ('9', '9:00 AM'), ('10', '10:00 AM'), ('11', '11:00 AM'),
        ('12', '12:00 PM'), ('13', '1:00 PM'), ('14', '2:00 PM'),
        ('15', '3:00 PM'), ('16', '4:00 PM'), ('17', '5:00 PM'),
        ('18', '6:00 PM'), ('19', '7:00 PM'), ('20', '8:00 PM'),
        ('21', '9:00 PM'), ('22', '10:00 PM'), ('23', '11:00 PM'),
]


class NotificationTeamApprover(models.Model):
    _name = "notification.team.approver"
    _description = "Notification validation"

    active = fields.Boolean(string="Active", default=True)

    user_id = fields.Many2one(
        'res.users',
        string="authorizer",
    )

    frequency = fields.Selection([
        ('daily', 'Daily'),
        ('weekdays', 'Week days')
    ], string='Frequency',
    )

    time = fields.Selection(
        TIMES, string='Hour',
    )

    days = fields.Many2many('days.week', string="Dias")

    is_purchase = fields.Boolean(string="Purchase order")

    def send_notification(self):
        user_tz = pytz.timezone(self.env.context.get('tz') or self.env.user.tz or 'UTC')
        date_now = datetime.now().astimezone(user_tz)
        notification_users_ids = self.env["notification.team.approver"].search([
            '&', '&', ("active", "=", True),
            '&', ("is_purchase", "=", True),
            ("time", "=", date_now.hour),
            '|', ("frequency", "=", "daily"),
            ("days.code", "=", date_now.weekday())
        ]).mapped("user_id")
        for user in notification_users_ids:
            template = 'add_notification_request_approval_route.notification_purchases_to_approve'
            order_ids = self.env["purchase.order"].search([
                ("current_approver.user_id", "=", user.id)
            ])
            self.send_mail(template, user, order_ids)

    def send_mail(self, template, user, order_id):
        order_id.with_user(user).message_post_with_view(
            template,
            composition_mode='mass_mail',
            partner_ids=[(4, user.partner_id.id)],
            auto_delete=True,
            auto_delete_message=True,
            parent_id=False,
            subtype_id=self.env.ref('mail.mt_note').id)


class DaysWeek(models.Model):
    _name = 'days.week'

    code = fields.Char('code')
    name = fields.Char('name')

    @api.model
    def _return_digit(self):
        for rec in self:
            if rec.name == 'Lunes':
                return 0
            elif rec.name == 'Martes':
                return 1
            elif rec.name == 'Miercoles':
                return 2
            elif rec.name == 'Jueves':
                return 3
            elif rec.name == 'Viernes':
                return 4
            elif rec.name == 'Sabado':
                return 5
            elif rec.name == 'Domingo':
                return 6
